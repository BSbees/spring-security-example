<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title>Welcome page</title>
</head>
<body>
	<h2>Home page</h2>
	<hr>
	<p>Welcome to the home page. YOHOO!!!</p>
	<hr>
	<p>User: <security:authentication property="principal.username"/><br>
		Roles: <security:authentication property="principal.authorities"/><br>
	<hr>
	<security:authorize access="hasRole('Manager')">
	<a href="${pageContext.request.contextPath }/leaders">Leaders section</a>
	<hr>
	</security:authorize>
	<security:authorize access="hasRole('Admin')">
	<a href="${pageContext.request.contextPath }/systems">Admin section</a>
	<hr>
	</security:authorize>
	<!-- logout button -->
	<form:form action="${pageContext.request.contextPath }/logout" method="post">
		<input type="submit" value="logout"/>
	</form:form>
</body></html>