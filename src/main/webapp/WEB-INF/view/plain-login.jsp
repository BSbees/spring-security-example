<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<title>Custom login page</title>
<style>
.failed {
	color: red;
}
</style>
<body>
	<h3>My custom login page</h3>
	<c:if test="${param.error != null }">
		<i class="failed">Incorrect username / password</i>
	</c:if>
	<form:form
		action="${pageContext.request.contextPath }/authenticateTheUser"
		method="post">
		<p>
			User name: <input type="text" name="username" /> <br /> Password: <input
				type="password" name="password" /> <BR /> <input type="submit"
				value="submit" />
		</p>

	</form:form>
</body>
</html>