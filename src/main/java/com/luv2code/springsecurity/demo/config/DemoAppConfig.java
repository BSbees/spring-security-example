package com.luv2code.springsecurity.demo.config;

import java.beans.PropertyVetoException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableWebMvc
@ComponentScan("com.luv2code.springsecurity.demo")
@PropertySource("classpath:persistence-mysql.properties")
public class DemoAppConfig {
	
	//variable holding properties
	@Autowired
	private Environment env;
	
	private Logger logger = Logger.getLogger(DemoAppConfig.class.getName());
	
	@Bean
	public ViewResolver viewResolver(){
		
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver("/WEB-INF/view/", ".jsp");
		return viewResolver;
	}
	
	@Bean
	public DataSource securityDataSource(){
		
		//create conn pool
		ComboPooledDataSource securityDataSource = new ComboPooledDataSource();
		
		//setjdbc class
		try {
			securityDataSource.setDriverClass(env.getProperty("jdbc.driver"));
		} catch (PropertyVetoException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		logger.info("===> jdbc.url = " + env.getProperty("jdbc.url"));
		logger.info("===> jdbc.user = " + env.getProperty("jdbc.user"));
		
		//set db conn props
		securityDataSource.setJdbcUrl(env.getProperty("jdbc.url"));
		securityDataSource.setUser(env.getProperty("jdbc.user"));
		securityDataSource.setPassword(env.getProperty("jdbc.password"));
		
		//set conn pool props
		securityDataSource.setInitialPoolSize(getIntPropetry("connection.pool.initialPoolSize"));
		securityDataSource.setMinPoolSize(getIntPropetry("connection.pool.minPoolSize"));
		securityDataSource.setMaxPoolSize(getIntPropetry("connection.pool.maxPoolSize"));
		securityDataSource.setMaxIdleTime(getIntPropetry("connection.pool.maxIdleTime"));
		
		
		return securityDataSource;
	}
	
	private int getIntPropetry(String path){
		return Integer.parseInt(env.getProperty(path));
	}
}
